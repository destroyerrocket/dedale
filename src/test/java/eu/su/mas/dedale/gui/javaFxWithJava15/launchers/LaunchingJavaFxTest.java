package eu.su.mas.dedale.gui.javaFxWithJava15.launchers;

import org.junit.Assert;

import eu.su.mas.dedale.gui.MyController;
import eu.su.mas.dedale.gui.javaFxWithJava15.testclass.BrowseTest;
import eu.su.mas.dedale.gui.javaFxWithJava15.testclass.JavaFxtest;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;

/**
Error: JavaFX runtime components are missing, and are required to run this application

This error is shown since the Java 15 launcher checks if the main class extends 
javafx.application.Application. If that is the case, it is required to have the 
javafx.graphics module on the module-path

If we can correct this if the program possess only one JavaFx main class (https://openjfx.io/openjfx-docs/#IDE-Eclipse),
 I did not find a proper way to do it if I have both javafx as a maven dependency and several JavaFx 
 classes (for tests use) */


public class LaunchingJavaFxTest {

//	public static void main(String[] args) {
//		new Thread(() -> {
//			Application.launch(JavaFxtest.class, null);
//		}).start();
//	}

	public static void main(String[] args) {
		JavaFxtest.main(args);
	}
	
}
