package eu.observables;

import java.util.Map;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class JavaFxTableViewExample extends Application {

	private ObserverOfAgents oa;
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws InterruptedException {

		TableView tableView = new TableView();

		TableColumn<AgentObservableElement, String> column1 = new TableColumn<>("Agent Name");
		column1.setCellValueFactory(new PropertyValueFactory<>("agentName"));


		TableColumn<AgentObservableElement, Integer> column2 = new TableColumn<>("Gold");
		column2.setCellValueFactory(new PropertyValueFactory<>("currentGoldValue"));

		TableColumn<AgentObservableElement, Integer> column3 = new TableColumn<>("Diamond");
		column3.setCellValueFactory(new PropertyValueFactory<>("currentDiamondValue"));

		// Set Sort type for userName column
	    column1.setSortType(TableColumn.SortType.ASCENDING);
	    
	 
		
		tableView.getColumns().add(column1);
		tableView.getColumns().add(column2);
		tableView.getColumns().add(column3);

		//tableView.getItems().add(new Person("John", "Doe"));
		//tableView.getItems().add(new Person("Jane", "Deer"));

		VBox vbox = new VBox(tableView);

		Scene scene = new Scene(vbox);

		primaryStage.setScene(scene);

		primaryStage.show();
		
		//Within the agent
		AgentObservableElement a=new AgentObservableElement("b1");
		AgentObservableElement b=new AgentObservableElement("Agent12");
		
		//sowhere (probbly GS)
		oa =new ObserverOfAgents();
		
		//when the agent is created
		oa.addAgent2Track("Agent12");//whithing the env
		
		b.addPropertyChangeListener(oa);//whithin the agent
		
		//within the Controller GUI
		tableView.getItems().add(a);
		tableView.getItems().add(new AgentObservableElement("agent2"));
		
		a.setCurrentDiamondValue(52);
		tableView.getItems().add(oa.getDataOn("Agent12"));
		
		b.setCurrentGoldValue(-1);
		
		
		
		
		
		
		
		
		
		
	}


}
