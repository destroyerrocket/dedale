package eu.su.mas.dedale.gui;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

/**
 * This class is used to capture the keys pressed within the GUI and notify the appropriate components
 * @author hc
 *
 */
public class KeyboardObservable implements Serializable{

	private static final long serialVersionUID = -7111423285890615845L;

	private String keyPressed;
	//private boolean isKeyPressed;
	
	private PropertyChangeSupport support;
	
	public KeyboardObservable() {
		this.keyPressed=null;
		//this.isKeyPressed=false;
		
		this.support = new PropertyChangeSupport(this);
		
	}
	
	public void addPropertyChangeListener(PropertyChangeListener pcl) {
		support.addPropertyChangeListener(pcl);
	}

	public void removePropertyChangeListener(PropertyChangeListener pcl) {
		support.removePropertyChangeListener(pcl);
	}
	
	public void setKeyPressed(String s) {
		support.firePropertyChange("GUI-controlledAgent", "", s);
		this.keyPressed=s;
	}
	
	public String getKeyPressed() {
		return this.keyPressed;
	}
}
