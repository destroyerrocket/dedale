package eu.su.mas.dedale.env.jme;

import java.util.List;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.ElementType;
import eu.su.mas.dedale.env.EntityCharacteristics;
import eu.su.mas.dedale.env.EntityType;
import eu.su.mas.dedale.env.GeneratorType;
import eu.su.mas.dedale.env.IEnvironment;
import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.env.Observation;

public class jmeEnvironment implements IEnvironment {



	@Override
	public void CreateEnvironment(GeneratorType g, String topologyConfigurationFilePath, String instanceConfiguration,
			boolean diamond, boolean gold, boolean well, Integer... topologyParameters) {
		// TODO Auto-generated method stub
		
	}
	
/**
 * @deprecated
 */
	public void CreateEnvironment(String topologyConfigurationFilePath, String instanceConfiguration, boolean isGrid,
			Integer envSize, boolean diamond, boolean gold, boolean well) {
		// TODO Auto-generated method stub
		
	}

	
	public void deployEntity(String entityName, EntityCharacteristics e, Location locationId) {
		// TODO Auto-generated method stub
		
	}

	
	public void removeEntity(String entityName, EntityCharacteristics e) {
		// TODO Auto-generated method stub
		
	}
	
	public Location getCurrentPosition(String agentName) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Couple<Location, List<Couple<Observation, Integer>>>> observe(Location currentPosition, String agentName) {
		// TODO Auto-generated method stub
		return null;
	}


	
	public Integer moveTo(String entityName, EntityCharacteristics ec, Location targetedPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public int pick(String entityName, Location location, ElementType e, Integer maxQuantity) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	public boolean isReachable(String senderName, String receiverName, int communicationReach) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean dropOff(Location location, ElementType e,Integer quantity) {
		// TODO Auto-generated method stub
		return false;
		
	}

	public boolean throwGrenade(String agentName, Location locationId) {
		// TODO Auto-generated method stub
		return false;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public boolean openLock(String entityName, Location location, ElementType e) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean closeLock(Location l) {
		return false;
	}




	


	

}
