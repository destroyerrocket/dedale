/**
 * 
 */
package eu.su.mas.dedale.env.gs;

import eu.su.mas.dedale.env.Location;

/**
 * Location component for the graphStream environment
 * @author hc
 *
 */
public class gsLocation implements Location {
	private String nodeId;

	private static final long serialVersionUID = 192266977093273239L;

	public gsLocation(String nodeId) {
		this.nodeId=nodeId;
	}
	
	@Override
	public String toString() {
		return  nodeId;
	}
	
	public String getLocationId() {
		return nodeId;
	}
	
	@Override
	public boolean equals(Location l) {
		return this.nodeId.equalsIgnoreCase(l.toString());
	}
	
	
}

