package eu.su.mas.dedale.princ;

import eu.su.mas.dedale.env.EnvironmentType;
import eu.su.mas.dedale.env.GeneratorType;

/**
 * 
 * @author hc
 *
 */
public final class ConfigurationFile {


	/************************************
	 ***********************************
	 *
	 * 1) Network and platform parameters
	 * 
	 ***********************************/

	public static boolean PLATFORMisDISTRIBUTED= false;
	public static boolean COMPUTERisMAIN= true;

	public static String PLATFORM_HOSTNAME="127.0.0.1";
	public static String PLATFORM_ID="Ithaq";

	public static Integer PLATFORM_PORT=8887;

	public static String LOCAL_CONTAINER_NAME=PLATFORM_ID+"_"+"container1";
	public static String LOCAL_CONTAINER2_NAME=PLATFORM_ID+"_"+"container2";
	public static String LOCAL_CONTAINER3_NAME=PLATFORM_ID+"_"+"container3";
	public static String LOCAL_CONTAINER4_NAME=PLATFORM_ID+"_"+"container4";


	/************************************
	 ************************************
	 *
	 * 2) Environment parameters 
	 * 
	 ************************************/
	
	/**
	 * 	The GateKeeper is in charge of the Platform and of the agents within, do not change its name.
	 */
	public static String DEFAULT_GATEKEEPER_NAME="GK";

	/**
	 * The environment is either based on GraphStream (2D discrete) or JME (3D continuous).
	 */
	public static EnvironmentType ENVIRONMENT_TYPE=EnvironmentType.GS;
	
	/**
	 * The environment is either manually designed (manual), or generated with a specific generator (dorogovstev, grid,barabasi)
	 */
	public static GeneratorType GENERATOR_TYPE=GeneratorType.MANUAL;

	
	

	/************************************
	 *
	 * 2-a) Environment parameters IF manually designed
	 * 
	 ************************************/

	/**
	 * Give the topology
	 */
	//public static String INSTANCE_TOPOLOGY=null;
	public static String INSTANCE_TOPOLOGY="src/test/java/resources/map2018-topology";
	//public static String INSTANCE_TOPOLOGY="src/test/java/resources/testJussieu5.dgs";//aquila2";//CentreParis4.dgs";//map2019-topologyExam1";
	//public static String INSTANCE_TOPOLOGY="src/test/java/resources/HouatTopology";


	/**
	 * Give the elements available on the map, if any
	 */
	//public static String INSTANCE_CONFIGURATION_ELEMENTS="src/test/java/resources/emptyMap";
	public static String INSTANCE_CONFIGURATION_ELEMENTS="src/test/java/resources/map2018-elements";
	//public static String INSTANCE_CONFIGURATION_ELEMENTS="src/test/java/resources/map2019-elementsExam1";
	//public static String INSTANCE_CONFIGURATION_ELEMENTS="src/test/java/resources/Houat-elements";


	/************************************
	 * 
	 * 
	 * 2-b) Environment parameters IF the environment is generated 
	 * 
	 * 
	 ***********************************/

	/**
	 * Size of the generated environment 
	 */
	public static Integer ENVIRONMENT_SIZE=10;
	public static Integer OPTIONAL_ADDITIONAL_ENVGENERATOR_PARAM1=1;//used by the BARABASI_ALBERT generator to control the branching factor of the graph 
	public static Integer[] GENERATOR_PARAMETERS= {ENVIRONMENT_SIZE,OPTIONAL_ADDITIONAL_ENVGENERATOR_PARAM1};

	/**
	 * Wumpus proximity detection radius when generated
	 */
	public static final Integer DEFAULT_DETECTION_RADIUS = 1;


	/**
	 * 	Agents communication radius when generated
	 */
	public static Integer DEFAULT_COMMUNICATION_REACH=3;

	/**
	 * Elements on the map when generated
	 */
	
	public static boolean ACTIVE_WELL=false;
	public static boolean ACTIVE_GOLD=true;
	public static boolean ACTIVE_DIAMOND=false;

	
	/************************************
	 ************************************
	 *
	 * 3) Agents characteristics
	 * 
	 ************************************/

	//public static String INSTANCE_CONFIGURATION_ENTITIES=null;
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/map2018-agentExplo";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/agentExplo-2";
	public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/agentExplo-2.json";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/agentExploSolo";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/agentExploCoop-2";	
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/map2018-agentTanker";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/map2018-agentCollect";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/map2018-entities";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/map2018-agentGolem";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/agentKeyboardControlled";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/agentKeyboardControlled.json";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/map2018-agentTankerCollect";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/map2019-entitiesExam1";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/agentObserveTest.json";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/map2018-agentCollect.json";
}
